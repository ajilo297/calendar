package com.ajiloommen.calendar.calendarViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.TextView;

import com.ajiloommen.calendar.Calendar;
import com.ajiloommen.calendar.R;
import com.ajiloommen.calendar.calendarModel.DayModel;
import com.ajiloommen.calendar.calendarModel.MonthModel;

import java.util.ArrayList;

/**
 * Created by ajilo on 19-11-2017.
 */

public class MonthView extends CardView {
    private static final String TAG = "MonthView";
    private Context context;
    private AttributeSet attrs;
    private MonthModel monthModel;
    private TextView month_title;
    private GridLayout day_container;
    private ArrayList<DayModel> days;
    private ArrayList<DayView> dayViews;

    public MonthView(@NonNull Context context) {
        super(context);
    }

    public MonthView(Context context, MonthModel monthModel) {
        super(context);
        initView(context,null,monthModel);
    }

    public MonthView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context,attrs,null);
    }

    private void initView(Context context, AttributeSet attrs, MonthModel monthModel) {
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.month_view, this);

        month_title = findViewById(R.id.month_title);
        day_container = findViewById(R.id.day_container);

        this.context = context;
        if (attrs != null) this.attrs = attrs;
        createView(context, monthModel);
    }

    private void createView(Context context, MonthModel monthModel) {
        this.context = context;
        if (monthModel != null) {
            this.monthModel = monthModel;
            this.days = monthModel.getDays();
            this.dayViews = getDayViews();
            updateView();
        }
    }

    private ArrayList<DayView> getDayViews() {
        dayViews = new ArrayList<>();

        dayViews.add((DayView) findViewById(R.id.r1c1));
        dayViews.add((DayView) findViewById(R.id.r1c2));
        dayViews.add((DayView) findViewById(R.id.r1c3));
        dayViews.add((DayView) findViewById(R.id.r1c4));
        dayViews.add((DayView) findViewById(R.id.r5c5));
        dayViews.add((DayView) findViewById(R.id.r1c6));
        dayViews.add((DayView) findViewById(R.id.r1c7));

        dayViews.add((DayView) findViewById(R.id.r2c1));
        dayViews.add((DayView) findViewById(R.id.r2c2));
        dayViews.add((DayView) findViewById(R.id.r2c3));
        dayViews.add((DayView) findViewById(R.id.r2c4));
        dayViews.add((DayView) findViewById(R.id.r1c5));
        dayViews.add((DayView) findViewById(R.id.r2c6));
        dayViews.add((DayView) findViewById(R.id.r2c7));

        dayViews.add((DayView) findViewById(R.id.r3c1));
        dayViews.add((DayView) findViewById(R.id.r3c2));
        dayViews.add((DayView) findViewById(R.id.r3c3));
        dayViews.add((DayView) findViewById(R.id.r3c4));
        dayViews.add((DayView) findViewById(R.id.r2c5));
        dayViews.add((DayView) findViewById(R.id.r3c6));
        dayViews.add((DayView) findViewById(R.id.r3c7));

        dayViews.add((DayView) findViewById(R.id.r4c1));
        dayViews.add((DayView) findViewById(R.id.r4c2));
        dayViews.add((DayView) findViewById(R.id.r4c3));
        dayViews.add((DayView) findViewById(R.id.r4c4));
        dayViews.add((DayView) findViewById(R.id.r3c5));
        dayViews.add((DayView) findViewById(R.id.r4c6));
        dayViews.add((DayView) findViewById(R.id.r4c7));

        dayViews.add((DayView) findViewById(R.id.r5c1));
        dayViews.add((DayView) findViewById(R.id.r5c2));
        dayViews.add((DayView) findViewById(R.id.r5c3));
        dayViews.add((DayView) findViewById(R.id.r5c4));
        dayViews.add((DayView) findViewById(R.id.r4c5));
        dayViews.add((DayView) findViewById(R.id.r5c6));
        dayViews.add((DayView) findViewById(R.id.r5c7));

        dayViews.add((DayView) findViewById(R.id.r6c1));
        dayViews.add((DayView) findViewById(R.id.r6c2));
        dayViews.add((DayView) findViewById(R.id.r6c3));
        dayViews.add((DayView) findViewById(R.id.r6c4));
        dayViews.add((DayView) findViewById(R.id.r6c5));
        dayViews.add((DayView) findViewById(R.id.r6c6));
        dayViews.add((DayView) findViewById(R.id.r6c7));

        return dayViews;
    }

    private void updateView() {
        month_title.setText(monthModel.getMonthName());
        for (int i = 0; i < Calendar.DAY_COUNT; i++) {
            DayModel dayModel = days.get(i);
            DayView dayView = dayViews.get(i);
            dayView.updateWithParams(context, dayModel);

            Log.e(TAG, "Loaded: " + "Month");
        }
    }

    public void updateWithParams(Context context, MonthModel monthModel) {
//        initView(context, null, monthModel);
        createView(context, monthModel);
    }
}
