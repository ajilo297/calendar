package com.ajiloommen.calendar.calendarViews;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.TextView;

import com.ajiloommen.calendar.R;
import com.ajiloommen.calendar.calendarModel.DayModel;

/**
 * Created by ajilo on 19-11-2017.
 */

public class DayView extends ConstraintLayout {
    private static final String TAG = "DayView";
    private Context context;
    private AttributeSet attrs;
    private DayModel dayModel;
    private TextView date;

    public DayView(Context context) {
        super(context);
        initView(context, null, null);
    }

    public DayView(Context context, DayModel dayModel) {
        super(context);
        initView(context, null, dayModel);
    }

    public DayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs, null);
    }

    private void initView(Context context, AttributeSet attrs, DayModel dayModel) {

        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.day_view, this);

        this.context = context;
        date = findViewById(R.id.date);
        if (attrs != null){ this.attrs = attrs;}
        createView(context, dayModel);
    }

    private void createView(Context context, DayModel dayModel) {
        this.context = context;
        if (dayModel != null) {
            this.dayModel = dayModel;
            updateView();
        }
    }

    private void updateView() {
        if (dayModel.getDate() != null) {
            int day = dayModel.getDate().getDay();
            String day_text = String.valueOf(day);
            date.setText(day_text);

            Log.e(TAG, "Loaded: " + dayModel.getDate().getDay());
        }
    }

    public void updateWithParams(Context context, DayModel dayModel) {
        createView(context, dayModel);
    }
}
