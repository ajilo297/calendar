package com.ajiloommen.calendar.calendarViews;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.GridLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.TextView;

import com.ajiloommen.calendar.Calendar;
import com.ajiloommen.calendar.R;
import com.ajiloommen.calendar.calendarModel.MonthModel;
import com.ajiloommen.calendar.calendarModel.YearModel;

import java.util.ArrayList;

/**
 * Created by ajilo on 19-11-2017.
 */

public class YearView extends ConstraintLayout {
    private static final String TAG = "YearView";
    private Context context;
    private TextView year_title;
    private GridLayout month_container;
    private AttributeSet attrs;
    private YearModel yearModel;
    private ArrayList<MonthModel> months;
    private ArrayList<MonthView> monthViews;

    public YearView(Context context) {
        super(context);
        initView(context, null, null);
    }

    public YearView(Context context, YearModel yearModel) {
        super(context);
        initView(context, null, yearModel);
    }

    public YearView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs, null);
    }

    private void initView(Context context, AttributeSet attrs, YearModel yearModel) {
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.year_view, this);

        this.context = context;
        year_title = findViewById(R.id.year_title);
        month_container = findViewById(R.id.month_container);

        if (attrs != null)
            this.attrs = attrs;

        populateView(context, yearModel);
    }

    private void populateView(Context context, YearModel yearModel) {
        this.context = context;
        if (yearModel != null) {
            this.yearModel = yearModel;
            this.months = yearModel.getMonths();
            this.monthViews = setupMonthViews();
            updateView();
        }
    }

    private ArrayList<MonthView> setupMonthViews() {
        monthViews = new ArrayList<>();

        monthViews.add((MonthView) findViewById(R.id.jan));
        monthViews.add((MonthView) findViewById(R.id.feb));
        monthViews.add((MonthView) findViewById(R.id.mar));
        monthViews.add((MonthView) findViewById(R.id.apr));
        monthViews.add((MonthView) findViewById(R.id.may));
        monthViews.add((MonthView) findViewById(R.id.jun));
        monthViews.add((MonthView) findViewById(R.id.jul));
        monthViews.add((MonthView) findViewById(R.id.aug));
        monthViews.add((MonthView) findViewById(R.id.sep));
        monthViews.add((MonthView) findViewById(R.id.oct));
        monthViews.add((MonthView) findViewById(R.id.nov));
        monthViews.add((MonthView) findViewById(R.id.dec));

        return monthViews;
    }

    public void updateWithParams(Context context, YearModel yearModel) {
//        initView(context, null, yearModel);
        populateView(context, yearModel);
    }

    private void updateView() {
        year_title.setText(yearModel.getYear_title());
        for (int i = 0; i < Calendar.MONTH_COUNT; i++) {
            MonthModel monthModel = months.get(i);
            MonthView monthView = monthViews.get(i);
            monthView.updateWithParams(context,monthModel);
            Log.e(TAG, "Loaded: " + yearModel.getYear_title());
        }
    }
}
