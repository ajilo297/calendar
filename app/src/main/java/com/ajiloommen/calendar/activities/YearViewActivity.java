package com.ajiloommen.calendar.activities;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.ajiloommen.calendar.Calendar;
import com.ajiloommen.calendar.R;
import com.ajiloommen.calendar.calendarModel.YearModel;
import com.ajiloommen.calendar.calendarViews.YearView;
import com.ajiloommen.calendar.dataModel.Date;

public class YearViewActivity extends AppCompatActivity {

    private static final String TAG = "YearViewActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_year_view);

        final Context context = YearViewActivity.this;

        final java.util.Calendar c = java.util.Calendar.getInstance();
        int year = c.get(java.util.Calendar.YEAR);
        final Date date = new Date(year);

        final YearView year_view = findViewById(R.id.year_view);

        year_view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                year_view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                Log.e(TAG, "LayoutListener fired");
                YearModel yearModel = new YearModel(date);
                year_view.updateWithParams(context, yearModel);
            }
        });

//        YearModel yearModel = new YearModel(date);
//        year_view.updateWithParams(context, yearModel);

        Log.e(TAG, "Executed");
    }
}
