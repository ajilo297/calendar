package com.ajiloommen.calendar.calendarModel;

import com.ajiloommen.calendar.dataModel.Date;

/**
 * Created by ajilo on 19-11-2017.
 */

public class DayModel {

    private Date date;

    public DayModel() {
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void initialise() {
        Date date = new Date(-1);
        date.setDay(-1);
    }
}

