package com.ajiloommen.calendar.calendarModel;


import com.ajiloommen.calendar.dataModel.Date;

import java.util.ArrayList;
import java.util.Calendar;

import static com.ajiloommen.calendar.Calendar.DAY_COUNT;
import static com.ajiloommen.calendar.Calendar.MONTH_LIST;

/**
 * Created by ajilo on 19-11-2017.
 */

public class MonthModel {

    private Date date;
    private int dayIndex;
    private int numberOfDays;
    private ArrayList<DayModel> days;
    private String monthName;


    public MonthModel(Date monthDate) {

        this.date = monthDate;
        this.monthName = MONTH_LIST[date.getMonth()];

        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, date.getYear());
        c.set(Calendar.MONTH, date.getMonth());
        c.set(Calendar.DAY_OF_MONTH, 1);

        dayIndex = c.get(Calendar.DAY_OF_WEEK) - 1;
        numberOfDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);

        initDays();
    }

    public String getMonthName() {
        return monthName;
    }

    private void initDays() {
        days = new ArrayList<>();
        for (int i = 0; i < DAY_COUNT; i ++) {
            DayModel dayModel = new DayModel();
            dayModel.initialise();
            days.add(dayModel);
        }

        for (int i = 0; i < numberOfDays ; i++) {
            DayModel dayModel = days.get(dayIndex);
            dayModel.setDate(new Date(date.getYear(), date.getMonth(), i + 1));
            dayIndex++;
        }
    }

    public ArrayList<DayModel> getDays() {
        return days;
    }
}
