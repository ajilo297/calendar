package com.ajiloommen.calendar.calendarModel;


import com.ajiloommen.calendar.dataModel.Date;

import java.util.ArrayList;

import static com.ajiloommen.calendar.Calendar.MONTH_COUNT;

/**
 * Created by ajilo on 19-11-2017.
 */

public class YearModel {
    private static final String TAG = "YearModel";
    private Date date;
    private ArrayList<Date> dates;
    private ArrayList<MonthModel> months;
    private String year_title;


    public YearModel(Date date) {
        this.date = date;
        initMonths();
    }

    private void initMonths() {
        dates = new ArrayList<>();
        for (int i = 0; i < MONTH_COUNT; i++) {
            Date monthDate = new Date(date.getYear());
            monthDate.setMonth(i);
            dates.add(monthDate);
        }

        months = new ArrayList<>();
        for (Date mDate : dates) {
            MonthModel month = new MonthModel(mDate);
            months.add(month);
        }
    }

    public ArrayList<MonthModel> getMonths() {
        return months;
    }

    public String getYear_title() {
        return String.valueOf(date.getYear());
    }
}
