package com.ajiloommen.calendar;

import android.app.Application;

/**
 * Created by ajilo on 19-11-2017.
 */

public class Calendar extends Application {
    public static final int MONTH_COUNT = 12;
    public static final int DAY_COUNT = 42;public static final String[] MONTH_LIST =
            new String[]{"January",
                    "February",
                    "March",
                    "April",
                    "May",
                    "June",
                    "July",
                    "August",
                    "September",
                    "October",
                    "November",
                    "December"};

}
